#ifndef __JSON_PARSER_H__
#define __JSON_PARSER_H__ "json_parser.h"

/**
 * @brief Simple and fast JSON tokenizer
 *
 * Streamed push-style JSON tokenizer.
 */

/**
 * @brief Tokenizer type
 */
typedef struct _json_tokenizer json_tokenizer;

/**
 * @brief Tokenizer flags
 */
typedef enum {
  /* Token type */
  json_object_open = 1,
  json_object_close = 2,
  json_array_open = 3,
  json_array_close = 4,
  json_kv_separate = 5,
  json_el_separate = 6,
  json_null_chunk = 7,
  json_bool_chunk = 8,
  json_number_chunk = 9,
  json_string_chunk = 10,
} json_tokenizer_flags;

/**
 * @brief The token callback
 *
 * @param t The tokenizer state object
 * @param flags The tokenizer flags
 * @param ptr The pointer to beginning of data chunk
 * @param len The length of data chunk
 * @return On success this callback must return 1. The 0 means processing error.
 */
typedef int json_tokenizer_cb(json_tokenizer *t, json_tokenizer_flags flags, const char *ptr, size_t len);

/**
 * @brief Tokenizer structure
 */
struct _json_tokenizer {
  /**
   * @brief The current state of tokenizer
   */
  unsigned int state: 5;
  
  /**
   * @brief The current atomic value type
   */
  unsigned int atom: 3;
};

/**
 * @brief The initializer function
 *
 * @param t The tokenizer state object
 * @param d The pointer to userdata
 */
void json_tokenizer_init(json_tokenizer *t);

#define json_tokenizer_success (-1)
#define json_tokenizer_failed(x) (x > -1)

/**
 * @brief The tokenizer function
 *
 * @param t The tokenizer state object
 * @param cb The tokenizer callback
 * @param ptr The pointer to beginning of source data
 * @param len The length of source data to tokenize
 * @return Function returns negative value (@p json_tokenizer_success) on success. The non-negative value means position of invalid token.
 *
 * Failed tokenizer must be re-initialized before next executing.
 */
int json_tokenizer_execute(json_tokenizer *t, json_tokenizer_cb *cb, const char *ptr, size_t len);

#endif/*__JSON_PARSER_H__*/
