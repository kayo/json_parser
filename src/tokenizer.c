#include <stddef.h>
#include "json_parser.h"

#ifdef __GNUC__
#define force_inline inline __attribute__((always_inline))
#else
#define force_inline inline
#endif

typedef struct {
  json_tokenizer *tokenizer; /* pointer to tokenizer */
  json_tokenizer_cb *cb; /* callback function */
  int res; /* value returned by callback */
  const char *ptr; /* pointer to current position */
  const char *atom_ptr; /* pointer to beginning of atom value */
} tokenize_runtime;

typedef void tokenize_function(tokenize_runtime *r);

static force_inline int space(char c) {
  return c == ' ' || ('\t' <= c && c <= '\r');
}

static force_inline int digit(char c) {
  return '0' <= c && c <= '9';
}

static force_inline int hex(char c) {
  return ('0' <= c && c <= '9') || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F');
}

#ifndef JSON_TOKENIZER_DEBUG
#define JSON_TOKENIZER_DEBUG 0
#endif

#if JSON_TOKENIZER_DEBUG
#include <stdio.h>
#define tokenizer_dlog(n, v) fprintf(stderr, "tokenizer :: " #n " " #v " :: `%s`\n", r->ptr);
#else
#define tokenizer_dlog(n, v)
#endif

#define tokenizer_goto_(t, n)    \
  ((t)->state = state_##n)

#define tokenizer_goto(n) {           \
    tokenizer_dlog(goto, n);          \
    tokenizer_goto_(r->tokenizer, n); \
  }

#define tokenizer_call(n) {           \
    tokenizer_dlog(call, n);          \
    tokenize_##n(r);                  \
  }

#define tokenizer_fail_(t) ((t)->state = state_invalid)
#define tokenizer_fail() tokenizer_fail_(r->tokenizer)

#define tokenizer_emit_(t, cb, n) \
  ((cb)((t), json_##n, NULL, 0))

#define tokenizer_emit(n) {                           \
    tokenizer_dlog(emit, n);                          \
    r->res = tokenizer_emit_(r->tokenizer, r->cb, n); \
  }

#define tokenizer_back() (r->ptr--)
#define tokenizer_char (*r->ptr)

#define tokenizer_atoms(tokenizer_atom)     \
  tokenizer_atom(null)                      \
  tokenizer_atom(bool)                      \
  tokenizer_atom(number)                    \
  tokenizer_atom(string)

enum {
  atom_undefined = 0,
#define tokenizer_atom(n) atom_##n,
  tokenizer_atoms(tokenizer_atom)
#undef tokenizer_atom
};

static void tokenizer_atom_new_(tokenize_runtime *r, unsigned int a) {
  tokenizer_dlog(atom, new);
  r->tokenizer->atom = a;
  r->atom_ptr = r->ptr;
}

#define tokenizer_atom_new(n)                   \
  tokenizer_atom_new_(r, atom_##n)

static int tokenizer_atom_call_(tokenize_runtime *r, const char *ptr, size_t len) {
  json_tokenizer *t = r->tokenizer;
  switch (t->atom) {
#define tokenizer_atom(n) case atom_##n: return r->cb(t, json_##n##_chunk, ptr, len);
    tokenizer_atoms(tokenizer_atom)
#undef tokenizer_atom
  }
  return 0;
}

static void tokenizer_atom_end_(tokenize_runtime *r, int off) {
  tokenizer_dlog(atom, end);
  json_tokenizer *t = r->tokenizer;
  if (t->atom != atom_undefined) {
    size_t len = r->ptr - r->atom_ptr + off;
    tokenizer_atom_call_(r, r->atom_ptr, len);
    t->atom = atom_undefined;
  }
}

#define tokenizer_atom_end()                    \
  tokenizer_atom_end_(r, 0)

#define tokenizer_atom_end1()                   \
  tokenizer_atom_end_(r, 1)

#define tokenizer_skip_space() \
  if (!space(tokenizer_char))  \
    tokenizer_fail()

#define tokenizer_states(tokenizer_state)       \
  tokenizer_state(null_token2)                  \
  tokenizer_state(null_token1)                  \
  tokenizer_state(null_token)                   \
  tokenizer_state(true_token2)                  \
  tokenizer_state(true_token1)                  \
  tokenizer_state(true_token)                   \
  tokenizer_state(false_token3)                 \
  tokenizer_state(false_token2)                 \
  tokenizer_state(false_token1)                 \
  tokenizer_state(false_token)                  \
  tokenizer_state(number_end)                   \
  tokenizer_state(number_token_after_exp2)      \
  tokenizer_state(number_token_after_exp1)      \
  tokenizer_state(number_token_after_exp)       \
  tokenizer_state(number_token_after_dot1)      \
  tokenizer_state(number_token_after_dot)       \
  tokenizer_state(number_token_null1)           \
  tokenizer_state(number_token1)                \
  tokenizer_state(number_token)                 \
  tokenizer_state(number_token0)                \
  tokenizer_state(string_token1)                \
  tokenizer_state(string_token_unicode3)        \
  tokenizer_state(string_token_unicode2)        \
  tokenizer_state(string_token_unicode1)        \
  tokenizer_state(string_token_unicode)         \
  tokenizer_state(string_escape)                \
  tokenizer_state(string_token)                 \
  tokenizer_state(separator)                    \
  tokenizer_state(value)

enum {
  state_invalid = 0,
#define tokenizer_state(n) state_##n,
  tokenizer_states(tokenizer_state)
#undef tokenizer_state
};

#define tokenizer_state(n) static force_inline void tokenize_##n(tokenize_runtime *r)

tokenizer_state(value);
tokenizer_state(separator);

tokenizer_state(null_token2) {
  switch (tokenizer_char) {
  case 'l':
    tokenizer_atom_end1();
    tokenizer_goto(separator);
    break;
  default:
    tokenizer_fail();
  }
}

tokenizer_state(null_token1) {
  switch (tokenizer_char) {
  case 'l':
    tokenizer_goto(null_token2);
    break;
  default:
    tokenizer_fail();
  }
}

tokenizer_state(null_token) {
  switch (tokenizer_char) {
  case 'u':
    tokenizer_goto(null_token1);
    break;
  default:
    tokenizer_fail();
  }
}

tokenizer_state(true_token2) {
  switch (tokenizer_char) {
  case 'e':
    tokenizer_atom_end1();
    tokenizer_goto(separator);
    break;
  default:
    tokenizer_fail();
  }
}

tokenizer_state(true_token1) {
  switch (tokenizer_char) {
  case 'u':
    tokenizer_goto(true_token2);
    break;
  default:
    tokenizer_fail();
  }
}

tokenizer_state(true_token) {
  switch (tokenizer_char) {
  case 'r':
    tokenizer_goto(true_token1);
    break;
  default:
    tokenizer_fail();
  }
}

tokenizer_state(false_token3) {
  switch (tokenizer_char) {
  case 'e':
    tokenizer_atom_end1();
    tokenizer_goto(separator);
    break;
  default:
    tokenizer_fail();
  }
}

tokenizer_state(false_token2) {
  switch (tokenizer_char) {
  case 's':
    tokenizer_goto(false_token3);
    break;
  default:
    tokenizer_fail();
  }
}

tokenizer_state(false_token1) {
  switch (tokenizer_char) {
  case 'l':
    tokenizer_goto(false_token2);
    break;
  default:
    tokenizer_fail();
  }
}

tokenizer_state(false_token) {
  switch (tokenizer_char) {
  case 'a':
    tokenizer_goto(false_token1);
    break;
  default:
    tokenizer_fail();
  }
}

tokenizer_state(number_end) {
  tokenizer_atom_end();
  tokenizer_goto(separator);
  tokenizer_back();
}

#define tokenizer_number_end() { \
    tokenizer_back();            \
    tokenizer_goto(number_end);  \
  }

tokenizer_state(number_token_after_exp2) {
  switch (tokenizer_char) {
  case ',':
  case '}':
  case ']':
    tokenizer_number_end();
    break;
  default:
    if (digit(tokenizer_char)) {
      return;
    } else if (space(tokenizer_char)) {
      tokenizer_number_end();
    } else {
      tokenizer_fail();
    }
  }
}

tokenizer_state(number_token_after_exp1) {
  if (digit(tokenizer_char)) {
    tokenizer_goto(number_token_after_exp2);
  } else {
    tokenizer_fail();
  }
}

tokenizer_state(number_token_after_exp) {
  switch (tokenizer_char) {
  case '-':
  case '+':
    tokenizer_goto(number_token_after_exp1);
    break;
  default:
    if (digit(tokenizer_char)) {
      tokenizer_goto(number_token_after_exp2);
    } else {
      tokenizer_fail();
    }
  }
}

tokenizer_state(number_token_after_dot1) {
  switch (tokenizer_char) {
  case 'e':
  case 'E':
    tokenizer_goto(number_token_after_exp);
    break;
  case ',':
  case '}':
  case ']':
    tokenizer_number_end();
    break;
  default:
    if (digit(tokenizer_char)) {
      return;
    } else if (space(tokenizer_char)) {
      tokenizer_number_end();
    } else {
      tokenizer_fail();
    }
  }
}

tokenizer_state(number_token_after_dot) {
  if (digit(tokenizer_char)) {
    tokenizer_goto(number_token_after_dot1);
  } else {
    tokenizer_fail();
  }
}

tokenizer_state(number_token_null1) {
  switch (tokenizer_char) {
  case '.':
    tokenizer_goto(number_token_after_dot);
    break;
  case 'e':
  case 'E':
    tokenizer_goto(number_token_after_exp);
    break;
  case ',':
  case '}':
  case ']':
    tokenizer_number_end();
    break;
  default:
    if (space(tokenizer_char)) {
      tokenizer_number_end();
    } else {
      tokenizer_fail();
    }
  }
}

tokenizer_state(number_token1) {
  switch (tokenizer_char) {
  case '.':
    tokenizer_goto(number_token_after_dot);
    break;
  case 'e':
  case 'E':
    tokenizer_goto(number_token_after_exp);
    break;
  case ',':
  case '}':
  case ']':
    tokenizer_number_end();
    break;
  default:
    if (digit(tokenizer_char)) {
      return;
    } else if (space(tokenizer_char)) {
      tokenizer_number_end();
    } else {
      tokenizer_fail();
    }
  }
}

tokenizer_state(number_token) {
  if (digit(tokenizer_char)) {
    tokenizer_goto(number_token1);
  } else if(space(tokenizer_char)) {
    tokenizer_number_end();
  } else {
    tokenizer_fail();
  }
}

tokenizer_state(number_token0) {
  switch (tokenizer_char) {
  case '0':
    tokenizer_goto(number_token_null1);
    break;
  default:
    tokenizer_call(number_token);
  }
}

tokenizer_state(string_escape);

tokenizer_state(string_token1) {
  switch (tokenizer_char) {
  case '\\':
    tokenizer_goto(string_escape);
    break;
  case '"':
    tokenizer_atom_end();
    tokenizer_goto(separator);
    break;
  }
}

tokenizer_state(string_token_unicode3) {
  if (hex(tokenizer_char)) {
    tokenizer_goto(string_token1);
  } else {
    tokenizer_fail();
  }
}

tokenizer_state(string_token_unicode2) {
  if (hex(tokenizer_char)) {
    tokenizer_goto(string_token_unicode3);
  } else {
    tokenizer_fail();
  }
}

tokenizer_state(string_token_unicode1) {
  if (hex(tokenizer_char)) {
    tokenizer_goto(string_token_unicode2);
  } else {
    tokenizer_fail();
  }
}

tokenizer_state(string_token_unicode) {
  if (hex(tokenizer_char)) {
    tokenizer_goto(string_token_unicode1);
  } else {
    tokenizer_fail();
  }
}

tokenizer_state(string_escape) {
  switch (tokenizer_char) {
  case '"':
  case '\\':
  case '/':
  case 'b':
  case 'f':
  case 'n':
  case 'r':
  case 't':
    tokenizer_goto(string_token1);
    break;
  case 'u':
    tokenizer_goto(string_token_unicode);
    break;
  default:
    tokenizer_fail();
  }
}

tokenizer_state(string_token) {
  tokenizer_atom_new(string);
  tokenizer_goto(string_token1);
  tokenizer_call(string_token1);
}

tokenizer_state(separator) {
  switch (tokenizer_char) {
  case ',':
    tokenizer_emit(el_separate);
    tokenizer_goto(value);
    break;
  case ':':
    tokenizer_emit(kv_separate);
    tokenizer_goto(value);
    break;
  case '}':
    tokenizer_emit(object_close);
    break;
  case ']':
    tokenizer_emit(array_close);
    break;
  default:
    tokenizer_skip_space();
  }
}

tokenizer_state(value) {
  switch (tokenizer_char) {
  case '{':
    tokenizer_emit(object_open);
    break;
  case '[':
    tokenizer_emit(array_open);
    break;
  case '}':
    tokenizer_emit(object_close);
    tokenizer_goto(separator);
    break;
  case ']':
    tokenizer_emit(array_close);
    tokenizer_goto(separator);
    break;
  case 'n':
    tokenizer_atom_new(null);
    tokenizer_goto(null_token);
    break;
  case 't':
    tokenizer_atom_new(bool);
    tokenizer_goto(true_token);
    break;
  case 'f':
    tokenizer_atom_new(bool);
    tokenizer_goto(false_token);
    break;
  case '"':
    tokenizer_goto(string_token);
    break;
  case '-':
    tokenizer_atom_new(number);
    tokenizer_goto(number_token0);
    break;
  case '0':
    tokenizer_atom_new(number);
    tokenizer_goto(number_token_null1);
    break;
  default:
    if (digit(tokenizer_char)){
      tokenizer_atom_new(number);
      tokenizer_goto(number_token1);
    } else {
      tokenizer_skip_space();
    }
    break;
  }
}

void json_tokenizer_init(json_tokenizer *t) {
  t->state = state_value;
  t->atom = atom_undefined;
}

int json_tokenizer_execute(json_tokenizer *t, json_tokenizer_cb *cb, const char *ptr, size_t len) {
  if (t->state == state_invalid)
    return -1;
  
  tokenize_runtime r = {
    t,
    cb,
    1,
    ptr,
    ptr,
  };
  
  const char *end = ptr + len;
  
  for (; r.ptr < end; r.ptr++) {
    switch (t->state) {
#define _state_(n) case state_##n: tokenize_##n(&r); break;
      tokenizer_states(_state_)
#undef _state_
    }

    if (t->state == state_invalid || !r.res)
      goto failed;
  }
  
  if (t->atom != atom_undefined && r.ptr - r.atom_ptr > 0)
    if (!tokenizer_atom_call_(&r, r.atom_ptr, r.ptr - r.atom_ptr))
      goto failed;
  
  return json_tokenizer_success;
  
 failed:
  return r.ptr - ptr;
}
