test.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

test.list ?= test_tokenizer

define JSON_PARSER_TEST_RULES
TARGET.LIBS += test/$(1)
test/$(1).INHERIT := libjson_parser
test/$(1).SRCS += $(test.BASEPATH)$(1).c

TARGET.BINS += test/$(1)
test/$(1).DEPLIBS* := test/$(1)

test: test.$(1)
test.$(1): run.test/$(1)
endef

$(call ADDRULES,JSON_PARSER_TEST_RULES:test.list)
