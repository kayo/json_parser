#include <stddef.h>
#include <json_parser.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN32

#include <windows.h>
static inline uint64_t get_time(void) {
  LARGE_INTEGER t, f;
  QueryPerformanceCounter(&t);
  QueryPerformanceFrequency(&f);
  return t.QuadPart * 1e9 / f.QuadPart;
}

#define print_rusage()
#else

#include <time.h>

static inline uint64_t get_time(void) {
  struct timespec t;
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t);
  return t.tv_sec * 1e9 + t.tv_nsec;
}

#include <sys/time.h>
#include <sys/resource.h>

static inline uint64_t nS(struct timeval *tvp) {
  return tvp->tv_sec * 10e6 + tvp->tv_usec;
}

static inline void print_rusage(void) {
  struct rusage r;
  getrusage(RUSAGE_SELF, &r);
  printf("utime\tstime\tmaxrss\tixrss\tidrss\tisrss\n"
         "%lu uS\t%lu uS\t%lu\t%lu\t%lu\t%lu\n",
         nS(&r.ru_utime),
         nS(&r.ru_stime),
         r.ru_maxrss,
         r.ru_ixrss,
         r.ru_idrss,
         r.ru_isrss);
}
#endif

typedef enum {
  end_token = 0,
  object_open,
  object_close,
  array_open,
  array_close,
  kv_separate,
  el_separate,
  atom_chunk,
} test_token_type;

typedef enum {
  none_atom = 0,
  null_atom,
  bool_atom,
  number_atom,
  string_atom,
} test_atom_type;

typedef struct {
  test_token_type type;
  test_atom_type atom_type;
  const char *atom;
} test_token;

typedef struct {
  const test_token *token;
  int failed;
} test_state;

typedef struct {
  json_tokenizer tokenizer;
  test_state state;
} test_tokenizer;

const char *token_name[] = {
  "_",
  "{",
  "}",
  "[",
  "]",
  ":",
  ",",
  "*"
};

const char *atom_name[] = {
  "none",
  "null",
  "bool",
  "number",
  "string",
};

#define info(t) fprintf(stderr, "\e[1;37m.... ??\e[0m \e[1;34m%s\e[0m ", t)
#define pass(d) fprintf(stderr, "\e[1;35m[%lu nS]\r\e[1;32mPASS (:\e[0m\n", d)
#define fail(f, ...) fprintf(stderr, "\r\e[1;31mFAIL ):\e[0m\n" f "\n", ##__VA_ARGS__)

#define check_overrun(s)                 \
  if ((s)->token->type == end_token) {   \
    (s)->failed = 1;                     \
    fail("Overrun!");                    \
    return 0;                            \
  }

#define check_underrun(s)                \
  if ((s)->token->type != end_token) {   \
    (s)->failed = 1;                     \
    fail("Underrun!");                   \
    return 0;                            \
  }

static int check_token(json_tokenizer *t, test_token_type type) {
  test_state *s = &((test_tokenizer*)t)->state;
  check_overrun(s);
  if (s->token->type != type) {
    s->failed = 1;
    fail("Token mismatch!\n"
         "  Expected `%s`\n"
         "  Actual `%s`",
         token_name[s->token->type],
         token_name[type]);
    return 0;
  }
  s->token++;
  return 1;
}

#define on_token(T)                             \
  case json_##T: return check_token(t, T)

static int check_atom(json_tokenizer *t, const char *ptr, size_t len, test_atom_type atom_type){
  if (check_token(t, atom_chunk)) {
    test_state *s = &((test_tokenizer*)t)->state;
    
    if (s->token[-1].atom_type != atom_type) {
      s->failed = 1;
      fail("Atom type mismatch!\n"
           "  Expected `%s`\n"
           "  Actual `%s`",
           atom_name[s->token[-1].atom_type],
           atom_name[atom_type]);
    }
    
    char *str = strndup(ptr, len);
    
    if (0 != strcmp(str, s->token[-1].atom)) {
      s->failed = 1;
      fail("Atom value mismatch!\n"
           "  Expected `%s`\n"
           "  Actual `%s`",
           s->token[-1].atom,
           str);
      
      free(str);
      return 0;
    }
    
    free(str);
    return 1;
  }
  return 0;
}

#define on_chunk(T)                                       \
  case json_##T##_chunk: return check_atom(t, ptr, len, T##_atom)

static int callback(json_tokenizer *t, json_tokenizer_flags flags, const char *ptr, size_t len) {
  switch (flags) {
    on_token(object_open);
    on_token(object_close);

    on_token(array_open);
    on_token(array_close);

    on_token(kv_separate);
    on_token(el_separate);
    
    on_chunk(null);
    on_chunk(bool);
    on_chunk(number);
    on_chunk(string);
  }
  return 0;
}

static int test_run(const char *title,
                    const char *sample,
                    size_t length,
                    int result,
                    const test_token *tokens) {
  info(title);

  test_tokenizer t = {
    .state = {
      tokens,
      0,
    }
  };
  
  json_tokenizer_init(&t.tokenizer);

  uint64_t s = get_time();
  int r = json_tokenizer_execute(&t.tokenizer, callback, sample, length);
  uint64_t d = get_time() - s;
  
  if (r == result) {
    check_underrun(&t.state);
    
    if (!t.state.failed)
      pass(d);
  } else {
    if (!t.state.failed)
      fail("Internal error. Position: %d", r);
    else
      fail("Position: %d", r);
  }
  
  return r;
}

#define t(x) {x, none_atom, NULL}
#define o(x) t(x##_open)
#define c(x) t(x##_close)
#define s(x) t(x##_separate)
#define a(x, y) {atom_chunk, x##_atom, y}

#define test(title, sample, result, ...) {    \
    const test_token tokens[] = {             \
      __VA_ARGS__,                            \
      {end_token, none_atom, NULL}            \
    };                                        \
    test_run(title, sample, sizeof(sample)-1, \
             result, tokens);                 \
  }

static void atomic_tests(void) {
  test("Null value",
       " null ",
       json_tokenizer_success,
       a(null, "null"));

  test("Null value",
       " null ",
       json_tokenizer_success,
       a(null, "null"));
  
  test("Null value",
       " null ",
       json_tokenizer_success,
       a(null, "null"));
  
  test("False value",
       "false ",
       json_tokenizer_success,
       a(bool, "false"));

  test("True value",
       " true",
       json_tokenizer_success,
       a(bool, "true"));

  test("Null number",
       " 0 ",
       json_tokenizer_success,
       a(number, "0"));

  test("Natural number",
       "123",
       json_tokenizer_success,
       a(number, "123"));
  
  test("Negative number",
       "-45",
       json_tokenizer_success,
       a(number, "-45"));

  test("Floating number <1",
       "0.101",
       json_tokenizer_success,
       a(number, "0.101"));

  test("Floating number <<1",
       "0.0030",
       json_tokenizer_success,
       a(number, "0.0030"));
  
  test("Floating number >1",
       "2.2",
       json_tokenizer_success,
       a(number, "2.2"));

  test("Floating number >>1",
       "49123.010",
       json_tokenizer_success,
       a(number, "49123.010"));

  test("Negative floating number",
       "-1222.0011",
       json_tokenizer_success,
       a(number, "-1222.0011"));

  test("Scientific number",
       "1e1",
       json_tokenizer_success,
       a(number, "1e1"));
  
  test("Scientific number e<0",
       "0.1e-1",
       json_tokenizer_success,
       a(number, "0.1e-1"));

  test("Scientific number E<<0",
       "1224.1E-1234",
       json_tokenizer_success,
       a(number, "1224.1E-1234"));

  test("Scientific number E>0",
       "-0.0001E12",
       json_tokenizer_success,
       a(number, "-0.0001E12"));

  test("Scientific number e>>0",
       "-1224.1e+1234",
       json_tokenizer_success,
       a(number, "-1224.1e+1234"));

  /* Edge tests */
  test("Clipped null",
       "{nul\0",
       4,
       o(object));

  test("Clipped true",
       "{tru\0",
       4,
       o(object));

  test("Clipped false",
       "{fa\0",
       3,
       o(object));
  
  test("Start from 0 #1",
       "0e1",
       json_tokenizer_success,
       a(number, "0e1"));

  test("Start from 0 #2",
       "0.1e1",
       json_tokenizer_success,
       a(number, "0.1e1"));
  
  test("Start from 0 #3",
       "[00",
       2,
       o(array));

  test("Start from 0 #4",
       "[ 0123",
       3,
       o(array));

  test("After point #1",
       "{123.",
       json_tokenizer_success,
       o(object), a(number, "123."));

  test("After point #2",
       " { 0.}",
       5,
       o(object));
  
  test("After point #3",
       " { 0.E}",
       5,
       o(object));
  
  test("After exponent #1",
       "{0E}",
       3,
       o(object));

  test("After exponent #2",
       "{0E-}",
       4,
       o(object));

  test("After exponent #3",
       "{-1e+}",
       5,
       o(object));

  test("Positive number",
       "{+12",
       1,
       o(object));
}

static void simple_tests(void) {
  test("Empty object",
       "{}",
       json_tokenizer_success,
       o(object),
       c(object));

  test("Empty array",
       "[]",
       json_tokenizer_success,
       o(array),
       c(array));
  
  test("Simple object string value",
       "{ \"v\":\"1\"}",
       json_tokenizer_success,
       o(object),
       a(string, "v"),
       s(kv),
       a(string, "1"),
       c(object));

  test("Space test",
       "{\"v\" :\t\"1\"\r\n}",
       json_tokenizer_success,
       o(object),
       a(string, "v"),
       s(kv),
       a(string, "1"),
       c(object));

  test("Simple object numeric value",
       "{\"v\"\t:\n1\r }",
       json_tokenizer_success,
       o(object),
       a(string, "v"),
       s(kv),
       a(number, "1"),
       c(object));

  test("Single quote in string",
       "{ \"'v\":\"ab'c\"}",
       json_tokenizer_success,
       o(object),
       a(string, "'v"),
       s(kv),
       a(string, "ab'c"),
       c(object));
  
  test("Object float value",
       "{\"PI\": 3.141E-10}",
       json_tokenizer_success,
       o(object),
       a(string, "PI"),
       s(kv),
       a(number, "3.141E-10"),
       c(object));

  test("Lowcase float value",
       "{ \"pi\":3.141e-10}",
       json_tokenizer_success,
       o(object),
       a(string, "pi"),
       s(kv),
       a(number, "3.141e-10"),
       c(object));

  test("Long number in array",
       "[12345123456789]",
       json_tokenizer_success,
       o(array),
       a(number, "12345123456789"),
       c(array));

  test("Bigint number in array",
       "[ 123456789123456789123456789 ]",
       json_tokenizer_success,
       o(array),
       a(number, "123456789123456789123456789"),
       c(array));

  test("Simple digit array",
       "[1,2,3,4]",
       json_tokenizer_success,
       o(array),
       a(number, "1"), s(el),
       a(number, "2"), s(el),
       a(number, "3"), s(el),
       a(number, "4"),
       c(array));

  test("String digit array",
       "[ \"1\", \"2\", \"3\", \"4\" ]",
       json_tokenizer_success,
       o(array),
       a(string, "1"), s(el),
       a(string, "2"), s(el),
       a(string, "3"), s(el),
       a(string, "4"),
       c(array));
  
  test("Array of empties",
       "[ { }, { },[]]",
       json_tokenizer_success,
       o(array),
       o(object), c(object), s(el),
       o(object), c(object), s(el),
       o(array), c(array),
       c(array));

  test("Lowercase Unicode Text",
       "{\"key\":\"\\u2000\\u20ff\"}",
       json_tokenizer_success,
       o(object),
       a(string, "key"), s(kv),
       a(string, "\\u2000\\u20ff"),
       c(object));

  test("Uppercase Unicode Text",
       " {\"key\" : \"\\u2000\\u20FF\"} ",
       json_tokenizer_success,
       o(object),
       a(string, "key"), s(kv),
       a(string, "\\u2000\\u20FF"),
       c(object));
  
  test("non protected / text",
       "{\"a\":\"hp://foo\"}",
       json_tokenizer_success,
       o(object),
       a(string, "a"), s(kv),
       a(string, "hp://foo"),
       c(object));

  test("Null in object",
       "{ \"a\":null}",
       json_tokenizer_success,
       o(object),
       a(string, "a"), s(kv),
       a(null, "null"),
       c(object));

  test("Boolean",
       "{ \"a\":true}",
       json_tokenizer_success,
       o(object),
       a(string, "a"), s(kv),
       a(bool, "true"),
       c(object));

  test("Non trimed data",
       "{ \"a\" : false }",
       json_tokenizer_success,
       o(object),
       a(string, "a"), s(kv),
       a(bool, "false"),
       c(object));
  
  test("Double floating",
       "{\"x\":1.7976931348623157E308}",
       json_tokenizer_success,
       o(object),
       a(string, "x"), s(kv),
       a(number, "1.7976931348623157E308"),
       c(object));
  
  test("Trucated key",
       "{\"X",
       json_tokenizer_success,
       o(object),
       a(string, "X"));
  
  test("Trucated value",
       "{\"X\":\"s",
       json_tokenizer_success,
       o(object),
       a(string, "X"), s(kv),
       a(string, "s"));
}

static void json_checker(void) {
  test("JSON Checker: pass1",
       "[\n"
       "    \"JSON Test Pattern pass1\",\n"
       "    {\"object with 1 member\":[\"array with 1 element\"]},\n"
       "    {},\n"
       "    [],\n"
       "    -42,\n"
       "    true,\n"
       "    false,\n"
       "    null,\n"
       "    {\n"
       "        \"integer\": 1234567890,\n"
       "        \"real\": -9876.543210,\n"
       "        \"e\": 0.123456789e-12,\n"
       "        \"E\": 1.234567890E+34,\n"
       "        \"\":  23456789012E66,\n"
       "        \"zero\": 0,\n"
       "        \"one\": 1,\n"
       "        \"space\": \" \",\n"
       "        \"quote\": \"\\\"\",\n"
       "        \"backslash\": \"\\\\\",\n"
       "        \"controls\": \"\\b\\f\\n\\r\\t\",\n"
       "        \"slash\": \"/ & \\/\",\n"
       "        \"alpha\": \"abcdefghijklmnopqrstuvwyz\",\n"
       "        \"ALPHA\": \"ABCDEFGHIJKLMNOPQRSTUVWYZ\",\n"
       "        \"digit\": \"0123456789\",\n"
       "        \"0123456789\": \"digit\",\n"
       "        \"special\": \"`1~!@#$%^&*()_+-={':[,]}|;.</>?\",\n"
       "        \"hex\": \"\\u0123\\u4567\\u89AB\\uCDEF\\uabcd\\uef4A\",\n"
       "        \"true\": true,\n"
       "        \"false\": false,\n"
       "        \"null\": null,\n"
       "        \"array\":[  ],\n"
       "        \"object\":{  },\n"
       "        \"address\": \"50 St. James Street\",\n"
       "        \"url\": \"http://www.JSON.org/\",\n"
       "        \"comment\": \"// /* <!-- --\",\n"
       "        \"# -- --> */\": \" \",\n"
       "        \" s p a c e d \" :[1,2 , 3\n"
       "\n"
       ",\n"
       "\n"
       "4 , 5        ,          6           ,7        ],\"compact\":[1,2,3,4,5,6,7],\n"
       "        \"jsontext\": \"{\\\"object with 1 member\\\":[\\\"array with 1 element\\\"]}\",\n"
       "        \"quotes\": \"&#34; \\u0022 %22 0x22 034 &#x22;\",\n"
       "        \"\\/\\\\\\\"\\uCAFE\\uBABE\\uAB98\\uFCDE\\ubcda\\uef4A\\b\\f\\n\\r\\t`1~!@#$%^&*()_+-=[]{}|;:',./<>?\"\n"
       ": \"A key can be any string\"\n"
       "    },\n"
       "    0.5 ,98.6\n"
       ",\n"
       "99.44\n"
       ",\n"
       "\n"
       "1066,\n"
       "1e1,\n"
       "0.1e1,\n"
       "1e-1,\n"
       "1e00,2e+00,2e-00\n"
       ",\"rosebud\"]",
       json_tokenizer_success,
       o(array),
       a(string, "JSON Test Pattern pass1"),
       s(el),
       o(object),
       a(string, "object with 1 member"),
       s(kv),
       o(array),
       a(string, "array with 1 element"),
       c(array),
       c(object),
       s(el),
       o(object), c(object), s(el),
       o(array), c(array), s(el),
       a(number, "-42"), s(el),
       a(bool, "true"), s(el),
       a(bool, "false"), s(el),
       a(null, "null"), s(el),
       o(object),
       a(string, "integer"), s(kv), a(number, "1234567890"), s(el),
       a(string, "real"), s(kv), a(number, "-9876.543210"), s(el),
       a(string, "e"), s(kv), a(number, "0.123456789e-12"), s(el),
       a(string, "E"), s(kv), a(number, "1.234567890E+34"), s(el),
       a(string, ""), s(kv), a(number, "23456789012E66"), s(el),
       a(string, "zero"), s(kv), a(number, "0"), s(el),
       a(string, "one"), s(kv), a(number, "1"), s(el),
       a(string, "space"), s(kv), a(string, " "), s(el),
       a(string, "quote"), s(kv), a(string, "\\\""), s(el),
       a(string, "backslash"), s(kv), a(string, "\\\\"), s(el),
       a(string, "controls"), s(kv), a(string, "\\b\\f\\n\\r\\t"), s(el),
       a(string, "slash"), s(kv), a(string, "/ & \\/"), s(el),
       a(string, "alpha"), s(kv), a(string, "abcdefghijklmnopqrstuvwyz"), s(el),
       a(string, "ALPHA"), s(kv), a(string, "ABCDEFGHIJKLMNOPQRSTUVWYZ"), s(el),
       a(string, "digit"), s(kv), a(string, "0123456789"), s(el),
       a(string, "0123456789"), s(kv), a(string, "digit"), s(el),
       a(string, "special"), s(kv), a(string, "`1~!@#$%^&*()_+-={':[,]}|;.</>?"), s(el),
       a(string, "hex"), s(kv), a(string, "\\u0123\\u4567\\u89AB\\uCDEF\\uabcd\\uef4A"), s(el),
       a(string, "true"), s(kv), a(bool, "true"), s(el),
       a(string, "false"), s(kv), a(bool, "false"), s(el),
       a(string, "null"), s(kv), a(null, "null"), s(el),
       a(string, "array"), s(kv), o(array), c(array), s(el),
       a(string, "object"), s(kv), o(object), c(object), s(el),
       a(string, "address"), s(kv), a(string, "50 St. James Street"), s(el),
       a(string, "url"), s(kv), a(string, "http://www.JSON.org/"), s(el),
       a(string, "comment"), s(kv), a(string, "// /* <!-- --"), s(el),
       a(string, "# -- --> */"), s(kv), a(string, " "), s(el),
       a(string, " s p a c e d "), s(kv), o(array),
       a(number, "1"), s(el),
       a(number, "2"), s(el),
       a(number, "3"), s(el),
       a(number, "4"), s(el),
       a(number, "5"), s(el),
       a(number, "6"), s(el),
       a(number, "7"),
       c(array), s(el),
       a(string, "compact"), s(kv),
       o(array),
       a(number, "1"), s(el),
       a(number, "2"), s(el),
       a(number, "3"), s(el),
       a(number, "4"), s(el),
       a(number, "5"), s(el),
       a(number, "6"), s(el),
       a(number, "7"),
       c(array), s(el),
       a(string, "jsontext"), s(kv), a(string, "{\\\"object with 1 member\\\":[\\\"array with 1 element\\\"]}"), s(el),
       a(string, "quotes"), s(kv), a(string, "&#34; \\u0022 %22 0x22 034 &#x22;"), s(el),
       a(string, "\\/\\\\\\\"\\uCAFE\\uBABE\\uAB98\\uFCDE\\ubcda\\uef4A\\b\\f\\n\\r\\t`1~!@#$%^&*()_+-=[]{}|;:',./<>?"), s(kv), a(string, "A key can be any string"),
       c(object), s(el),
       a(number, "0.5"), s(el),
       a(number, "98.6"), s(el),
       a(number, "99.44"), s(el),
       a(number, "1066"), s(el),
       a(number, "1e1"), s(el),
       a(number, "0.1e1"), s(el),
       a(number, "1e-1"), s(el),
       a(number, "1e00"), s(el),
       a(number, "2e+00"), s(el),
       a(number, "2e-00"), s(el),
       a(string, "rosebud"),
       c(array));
  
  test("JSON Checker: pass2",
       "[[[[[[[[[[[[[[[[[[[\"Not too deep\"]]]]]]]]]]]]]]]]]]]",
       json_tokenizer_success,
       o(array), o(array), o(array), o(array), o(array),
       o(array), o(array), o(array), o(array), o(array),
       o(array), o(array), o(array), o(array), o(array),
       o(array), o(array), o(array), o(array),
       a(string, "Not too deep"),
       c(array), c(array), c(array), c(array), c(array),
       c(array), c(array), c(array), c(array), c(array),
       c(array), c(array), c(array), c(array), c(array),
       c(array), c(array), c(array), c(array));

  test("JSON Checker: pass3",
       "{\n"
       "    \"JSON Test Pattern pass3\": {\n"
       "        \"The outermost value\": \"must be an object or array.\",\n"
       "        \"In this test\": \"It is an object.\"\n"
       "    }\n"
       "}\n",
       json_tokenizer_success,
       o(object),
       a(string, "JSON Test Pattern pass3"), s(kv),
       o(object),
       a(string, "The outermost value"), s(kv),
       a(string, "must be an object or array."), s(el),
       a(string, "In this test"), s(kv),
       a(string, "It is an object."),
       c(object),
       c(object));
  
  test("JSON Checker: fail1",
       "\"A JSON payload should be an object or array, not a string.\"",
       json_tokenizer_success,
       a(string, "A JSON payload should be an object or array, not a string."));
  
  test("JSON Checker: fail2",
       "[\"Unclosed array\"",
       json_tokenizer_success,
       o(array), a(string, "Unclosed array"));
  
  test("JSON Checker: fail3",
       "{unquoted_key: \"keys must be quoted\"}",
       1,
       o(object));

  test("JSON Checker: fail4",
       "[\"extra comma\",]",
       json_tokenizer_success,
       o(array), a(string, "extra comma"), s(el), c(array));

  test("JSON Checker: fail5",
       "[\"double extra comma\",,]",
       22,
       o(array), a(string, "double extra comma"), s(el));

  test("JSON Checker: fail6",
       "[   , \"<-- missing value\"]",
       4,
       o(array));

  test("JSON Checker: fail7",
       "[\"Comma after the close\"],",
       json_tokenizer_success,
       o(array),
       a(string, "Comma after the close"),
       c(array), s(el));

  test("JSON Checker: fail8",
       "[\"Extra close\"]]",
       json_tokenizer_success,
       o(array), a(string, "Extra close"),
       c(array), c(array));
  
  test("JSON Checker: fail9",
       "{\"Extra comma\": true,}",
       json_tokenizer_success,
       o(object), a(string, "Extra comma"), s(kv),
       a(bool, "true"), s(el), c(object));

  test("JSON Checker: fail10",
       "{\"Extra value after close\": true} \"misplaced quoted value\"",
       34,
       o(object), a(string, "Extra value after close"), s(kv),
       a(bool, "true"), c(object));

  test("JSON Checker: fail11",
       "{\"Illegal expression\": 1 + 2}",
       25,
       o(object), a(string, "Illegal expression"), s(kv),
       a(number, "1"));

  test("JSON Checker: fail12",
       "{\"Illegal invocation\": alert()}",
       23,
       o(object), a(string, "Illegal invocation"), s(kv));

  test("JSON Checker: fail13",
       "{\"Numbers cannot have leading zeroes\": 013}",
       40,
       o(object), a(string, "Numbers cannot have leading zeroes"), s(kv));

  test("JSON Checker: fail14",
       "{\"Numbers cannot be hex\": 0x14}",
       27,
       o(object), a(string, "Numbers cannot be hex"), s(kv));

  test("JSON Checker: fail15",
       "[\"Illegal backslash escape: \\x15\"]",
       29,
       o(array));

  test("JSON Checker: fail16",
       "[\\naked]",
       1,
       o(array));

  test("JSON Checker: fail17",
       "[\"Illegal backslash escape: \\017\"]",
       29,
       o(array));

  test("JSON Checker: fail18",
       "[[[[[[[[[[[[[[[[[[[[\"Too deep\"]]]]]]]]]]]]]]]]]]]]",
       json_tokenizer_success,
       o(array), o(array), o(array), o(array), o(array),
       o(array), o(array), o(array), o(array), o(array),
       o(array), o(array), o(array), o(array), o(array),
       o(array), o(array), o(array), o(array), o(array),
       a(string, "Too deep"),
       c(array), c(array), c(array), c(array), c(array),
       c(array), c(array), c(array), c(array), c(array),
       c(array), c(array), c(array), c(array), c(array),
       c(array), c(array), c(array), c(array), c(array));

  test("JSON Checker: fail19",
       "{\"Missing colon\" null}",
       17,
       o(object), a(string, "Missing colon"));

  test("JSON Checker: fail20",
       "{\"Double colon\":: null}",
       16,
       o(object), a(string, "Double colon"), s(kv));

  test("JSON Checker: fail21",
       "{\"Comma instead of colon\", null}",
       json_tokenizer_success,
       o(object), a(string, "Comma instead of colon"), s(el),
       a(null, "null"), c(object));

  test("JSON Checker: fail22",
       "[\"Colon instead of comma\": false]",
       json_tokenizer_success,
       o(array), a(string, "Colon instead of comma"), s(kv),
       a(bool, "false"), c(array));

  test("JSON Checker: fail23",
       "[\"Bad value\", truth]",
       17,
       o(array), a(string, "Bad value"), s(el));

  test("JSON Checker: fail24",
       "['single quote']",
       1,
       o(array));

  test("JSON Checker: fail25",
       "[\"\ttab\tcharacter\tin\tstring\t\"]",
       json_tokenizer_success,
       o(array), a(string, "	tab	character	in	string	"), c(array));

  test("JSON Checker: fail26",
       "[\"tab\\   character\\   in\\  string\\  \"]",
       6,
       o(array));

  test("JSON Checker: fail27",
       "[\"line\n"
       "break\"]",
       json_tokenizer_success,
       o(array), a(string, "line\nbreak"), c(array));

  test("JSON Checker: fail28",
       "[\"line\\\n"
       "break\"]",
       7,
       o(array));

  test("JSON Checker: fail29",
       "[0e]",
       3,
       o(array));

  test("JSON Checker: fail30",
       "[0e+]",
       4,
       o(array));

  test("JSON Checker: fail31",
       "[0e+-1]",
       4,
       o(array));

  test("JSON Checker: fail32",
       "{\"Comma instead if closing brace\": true,",
       json_tokenizer_success,
       o(object), a(string, "Comma instead if closing brace"), s(kv),
       a(bool, "true"), s(el));

  test("JSON Checker: fail33",
       "[\"mismatch\"}",
       json_tokenizer_success,
       o(array), a(string, "mismatch"), c(object));
}

int main(void) {
  uint64_t s = get_time();
  
  atomic_tests();
  simple_tests();
  json_checker();
  
  printf("\e[1;35m[%lu nS]\e[0m\n", get_time() - s);
  
  print_rusage();
  return 0;
}
