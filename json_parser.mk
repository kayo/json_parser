libjson_parser.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libjson_parser
libjson_parser.INHERIT := stalin
libjson_parser.CDIRS := $(libjson_parser.BASEPATH)include
libjson_parser.SRCS := $(addprefix $(libjson_parser.BASEPATH)src/,\
  tokenizer.c)

TARGET.DYNS += libjson_parser
libjson_parser.DYNVER = $(json_parser.version)
libjson_parser.DEPLIBS* := libjson_parser

json_parser.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.OPTS += json_parser
json_parser.OPTS := $(json_parser.BASEPATH)src/parser.cf
