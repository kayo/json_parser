example.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

example.list ?= message

define JSON_PARSER_EXAMPLE_RULES
TARGET.LIBS += example/$(1)
example/$(1).INHERIT := libjson_parser
example/$(1).SRCS += $(example.BASEPATH)$(1).c

TARGET.BINS += example/$(1)
example/$(1).DEPLIBS* := example/$(1)

example: example.$(1)
example.$(1): run.example/$(1)
endef

$(call ADDRULES,JSON_PARSER_EXAMPLE_RULES:example.list)
