#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include "json_parser.h"

typedef struct {
  unsigned int id;
  bool sent;
  char text[128];
} message_t;

typedef void message_cb(const message_t *message);

typedef enum {
  message_field_id = 0,
  message_field_sent,
  message_field_text,
} message_field_t;

static const char *message_field_s[] = {
  "id",
  "sent",
  "text"
};

typedef struct message_parser message_parser_t;

typedef int message_parser_cb(message_parser_t *s, const char *ptr, size_t len);

struct message_parser {
  json_tokenizer tokenizer;
  message_t *message;
  message_cb *on_message;
  message_parser_cb *parser;
  unsigned int offset;
  message_field_t field;
};

static int message_parser_id(message_parser_t *s, const char *ptr, size_t len) {
  const char *end = ptr + len;
  for (; ptr < end; ptr++) {
    if (*ptr < '0' || *ptr > '9') return 0; /* invalid id */
    s->message->id = 10 * s->message->id + (*ptr - '0');
  }
  return 1;
}

static int message_parser_sent(message_parser_t *s, const char *ptr, size_t len) {
  (void)len;
  if (s->offset == 0) {
    s->message->sent = *ptr == 't';
    s->offset = 1;
  }
  return 1;
}

static int message_parser_text(message_parser_t *s, const char *ptr, size_t len) {
  if (s->offset + len >= sizeof(s->message->text)) return 0; /* text is too long */
  memcpy(s->message->text + s->offset, ptr, len);
  s->offset += len;
  s->message->text[s->offset] = '\0';
  return 1;
}

static int message_parser_field(message_parser_t *s, const char *ptr, size_t len) {
  if (s->offset == 0) {
    unsigned int field = 0;
    for (field = 0; field < sizeof(message_field_s) / sizeof(message_field_s[0]); field++) {
      if (0 == strncmp(message_field_s[field], ptr, len)) {
        s->field = field;
        s->offset = len;
        return 1;
      }
    }
    return 0; /* unexpected field */
  } else {
    if (0 != strncmp(message_field_s[s->field] + s->offset, ptr, len)) {
      return 0; /* unexpected field */
    }
    s->offset += len;
  }
  return 1;
}

static int message_callback(json_tokenizer *t, json_tokenizer_flags flags, const char *ptr, size_t len) {
  message_parser_t *s = (message_parser_t*)t;
  
  switch (flags) {
  case json_object_open:
    s->parser = message_parser_field;
    s->offset = 0;
    return 1;
  case json_object_close:
    s->parser = NULL;
    s->offset = 0;
    if (s->on_message) s->on_message(s->message);
    return 1;
    
  case json_kv_separate:
    switch (s->field) {
    case message_field_id:
      s->parser = message_parser_id;
      s->message->id = 0;
      break;
    case message_field_sent:
      s->parser = message_parser_sent;
      s->message->sent = false;
      break;
    case message_field_text:
      s->parser = message_parser_text;
      s->message->text[0] = '\0';
      break;
    default: return 0;
    }
    s->offset = 0;
    return 1;

  case json_el_separate:
    s->parser = message_parser_field;
    s->offset = 0;
    return 1;

  case json_bool_chunk:
  case json_number_chunk:
  case json_string_chunk:
    if (!s->parser) return 0; /* invalid state */
    return s->parser(s, ptr, len);
    
  default:
    return 0;
  }
}

#include <stdio.h>

static void print_message(const message_t *message) {
  printf("parsed message: id=%d, sent=%s, text=%s\n", message->id, message->sent ? "yes" : "no", message->text);
}

int main(void) {
  message_t result_message;
  
  message_parser_t message_parser = {
    .message = &result_message,
    .on_message = print_message
  };
  
  const char *chunks[] = {
    NULL, "{\"id\":11, \"sent\" : true,\"text\":\"thunk thunk\"}",
    NULL, "{\"i", "d\"", ":1", "23", ",\"text\":\"", "example message\"", "}"
  };
  
  const char **chunk_ptr = chunks, **chunk_end = chunks + sizeof(chunks)/sizeof(chunks[0]);

  for (; chunk_ptr < chunk_end; chunk_ptr++) {
    if (*chunk_ptr == NULL) {
      result_message.id = 0;
      result_message.sent = false;
      result_message.text[0] = '\0';
      
      printf("init parser\n");
      json_tokenizer_init(&message_parser.tokenizer);
    } else {
      printf("push chunk: %s\n", *chunk_ptr);
      int result = json_tokenizer_execute(&message_parser.tokenizer, message_callback, *chunk_ptr, strlen(*chunk_ptr));
      if (result != json_tokenizer_success) {
        printf("parsing error at position: %d\n", result);
        return result;
      }
    }
  }
  
  return 0;
}
