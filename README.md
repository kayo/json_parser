# Simple static streamed JSON parser

This project is aimed to implementation of streamed parser for JSON data.

This project was inspired by *Igor Sysoev's* **http_parser** (also known as **libhttp-parser**).

## Overview

At the start point we had those reasons:

* The DOM[^1]-like parsers usually required heap allocations to store all parsed values. This fact is not good thing for embedded systems. The SAX[^2]-like parsers doesn't have this restriction.
* The DOM-like tokenizers[^3] usually operates on statically allocated data, but have problems with suspend/resume in case of partial/chunked input data. The SAX-like tokenizers doesn't have this problems.
* Usually we need to parse some number of source JSON fields, which required by our embedded application. The other fields we skip off.
* In embedded apps the document structure validation is less important than resource saving and simplicity of parser.

Another aspect is a difference between *pull-style* and *push-style* parsers. In our notion the pull-style parsers initiates reading of data for parsing yourself. Whereas push-styles parsers parse chunks of data at the moment when we feed it to it.

The pull-style parsers usually didn't fitted for asynchronous applications so good as push-style parsers.
But push-style parsers is a more complex, because it must save state between feeds. This project is *push-style* parser.

## Usage example

See *example/message.c* in source code repository.

*[DOM]: Document Object Model
*[SAX]: Simple API for XML
[^1]: Most of JSON parsers is a DOM-like parsers.
[^2]: This term usually related to XML-parsers but we use it with any callback-driven parser.
[^3]: Tokenizers differs from parser. Tokenizer extracts tokens from source and return raw data without any transformations.